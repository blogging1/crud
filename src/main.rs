#![feature(plugin)]
#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_derive_enum;
extern crate dotenv;
#[macro_use]
extern crate rocket;
extern crate rocket_contrib;
#[macro_use]
extern crate serde_derive;

use dotenv::dotenv;
use std::env;

pub mod db_pool;
pub mod order;
pub mod product;
pub mod schema;
pub mod specification;

fn main() {
    dotenv().ok();
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let database_max_size = env::var("DATABASE_MAX_SIZE")
        .unwrap_or(String::from("10"))
        .parse::<u32>()
        .unwrap();
    rocket::ignite()
        .manage(db_pool::init_pool(&database_url, database_max_size))
        .mount(
            "/orders",
            routes![
                order::create,
                order::get,
                order::start,
                order::cancel,
                order::get_all
            ],
        )
        .mount("/products", routes![product::get, product::get_all])
        .mount(
            "/specifications",
            routes![
                specification::create,
                specification::get,
                specification::find,
                specification::get_all,
                specification::update,
                specification::delete
            ],
        )
        .launch();
}

#[cfg(test)]
mod tests {

    use diesel::{prelude::*, result::Error, Connection};
    use dotenv::dotenv;
    use std::env;

    use super::db_pool::DbConn;
    use super::specification::*;
    use super::*;
    use rocket::http::{ContentType, Status};
    use rocket::local::Client;

    #[test]
    fn get_specification_by_correct_id_should_return_specification() {
        dotenv().ok();
        let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
        let database_max_size = env::var("DATABASE_MAX_SIZE")
            .unwrap_or(String::from("10"))
            .parse::<u32>()
            .unwrap();
        let conn = db_pool::init_pool(&database_url, database_max_size)
            .get()
            .unwrap();
        let rocket = rocket::ignite()
            .manage(db_pool::init_pool(&database_url, database_max_size))
            .mount("/specifications", routes![specification::get,]);
        let client = Client::new(rocket).expect("valid rocket instance");
        use super::schema::specifications::dsl::*;
        let specification: Specification = diesel::insert_into(specifications)
            .values(&NewSpecification {
                name: "Test".to_string(),
            })
            .get_result(&*conn)
            .expect("Error saving new specification");
        let path = format!("/specifications/{}", specification.id);
        let req = client.get(&path);
        let response = req.dispatch();
        assert_eq!(response.status(), Status::Ok);
    }
}
