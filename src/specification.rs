use super::schema::specifications;
use crate::db_pool::DbConn;
use diesel;
use diesel::prelude::*;
use rocket::http::RawStr;
use rocket::request::Form;
use rocket_contrib::json::Json;

#[derive(Serialize, Deserialize, Identifiable, Queryable, AsChangeset, Clone)]
#[table_name = "specifications"]
pub struct Specification {
    pub id: i32,
    pub name: String,
}

#[derive(Deserialize, Insertable)]
#[table_name = "specifications"]
pub struct NewSpecification {
    pub name: String,
}

#[post("/", data = "<specification>")]
pub fn create(specification: Json<NewSpecification>, conn: DbConn) -> Json<Specification> {
    use super::schema::specifications::dsl::*;
    let specification: NewSpecification = specification.0;
    Json(
        diesel::insert_into(specifications)
            .values(&specification)
            .get_result(&*conn)
            .expect("Error saving new specification"),
    )
}

#[get("/<fid>")]
pub fn get(fid: i32, conn: DbConn) -> Json<Specification> {
    use crate::schema::specifications::dsl::*;
    let specification = specifications
        .find(fid)
        .first::<Specification>(&*conn)
        .expect("Error loading specification");
    Json(specification)
}

#[get("/search?<spec>")]
pub fn find(spec: String, conn: DbConn) -> Json<Specification> {
    use super::schema::specifications::dsl::*;
    let specification = specifications
        .filter(name.eq(spec))
        .first::<Specification>(&*conn)
        .expect("Error loading specification");

    Json(specification)
}

#[get("/")]
pub fn get_all(conn: DbConn) -> QueryResult<Json<Vec<Specification>>> {
    use super::schema::specifications::dsl::*;
    specifications
        .load::<Specification>(&*conn)
        .map(|list| Json(list))
}

#[patch("/<fid>", data = "<specification>")]
pub fn update(fid: i32, specification: Json<Specification>, conn: DbConn) -> Json<Specification> {
    use super::schema::specifications::dsl::*;
    diesel::update(specifications.find(fid))
        .set(specification.into_inner())
        .execute(&*conn)
        .expect("Error updating specification");
    let result = specifications
        .find(fid)
        .first::<Specification>(&*conn)
        .expect("Error loading specification");

    Json(result)
}

#[delete("/<fid>")]
pub fn delete(fid: i32, conn: DbConn) {
    use super::schema::specifications::dsl::*;
    diesel::delete(specifications.find(fid))
        .execute(&*conn)
        .expect("Error deleting specification");
}
